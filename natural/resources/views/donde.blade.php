@extends('layout.main')

@section('title', 'Donde Comprar')

@section('content')

<!-- ======================================================= -->
<section id="where" class="main-section">
    <div class="title">
        <h1 class="front">¿Dónde Comprar?</h1>
    </div>
</section>
<section id="buy" data-aos="fade-up">
    <div class="title">
        <h1>Compra en</h1>
    </div>
    <div class="">
        <div class="prev-nav">
            <a href="#!" class="prev"><i class="fas fa-3x fa-chevron-left"></i></a>
        </div>
        <div class="buy-slider owl-carousel">
            <div class="slide">
                <a target="_blank" href="https://www.amazon.com.mx/s?k=Natural+Fit%2C+Aut%C3%A9ntico+Endulzante+de+Stevia&__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss"><img src="{{ asset('img/amazon.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar"><img src="{{ asset('img/walmart.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html"><img src="{{ asset('img/gnc.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/grand.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html"><img src="{{ asset('img/heb.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href=" https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit"><img src="{{ asset('img/logo-la-comer.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.merco.mx/"><img src="{{ asset('img/merco.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.superdelnorte.com.mx/"><img src="{{ asset('img/santovalle.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.supermercadossmart.com/"><img src="{{ asset('img/smart-logo.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.soriana.com/soriana/es/"><img src="{{ asset('img/soriana.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://sumesa.com.mx/sucursales.php"><img src="{{ asset('img/sumesa.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/vimark.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.casaley.com.mx/"><img src="{{ asset('img/ley.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+"><img src="{{ asset('img/al-super.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/avila.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.casaayala.mx/#"><img src="{{ asset('img/casa-ayala.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://abarrotescasavargas.mx/"><img src="{{ asset('img/casa-vargas.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=406&ver=mislistas&succFmt=100&criterio=natural+fit#/natural%20fit"><img src="{{ asset('img/logo-fresko.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/san-borja.png') }}" alt=""></a>
            </div>
        </div>
        <div class="next-nav">

            <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>
        </div>
    </div>
</section>
<section id="productos">
    <div class="producto-slider">
        <div class="owl-carousel">
            <div class="slide">

                <img src="{{ asset('img/stellar40.png') }}" alt="">

                <div class="subtitle">
                    <h3>Caja 40 Sobres</h3>
                    <a href="#!" target="_blank" class="button cancan">Comprar</a>
                </div>
            </div>
            <div class="slide">

                <img src="{{ asset('img/stellar100.png') }}" alt="">

                <div class="subtitle">
                    <h3>Caja 100 Sobres</h3>
                    <a href="#!" class="button cancan">Comprar</a>
                </div>
            </div>
            <div class="slide">

                <img src="{{ asset('img/stellar500.png') }}" alt="">

                <div class="subtitle">
                    <h3>Caja 500 Sobres</h3>
                    <a href="#!" class="button cancan">Comprar</a>
                </div>
            </div>
            <div class="slide">

                <img src="{{ asset('img/stellar2000.png') }}" alt="">

                <div class="subtitle">
                    <h3>Caja 2000 Sobres</h3>
                    <a href="#!" class="button cancan">Comprar</a>
                </div>
            </div>
        </div>
        <a href="#!" class="prev"><i class="fas fa-3x fa-chevron-left"></i></a>
        <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>
        <div class="producto-nav">
        </div>
    </div>
</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/where.css?v=4.1') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        jQuery( document ).ready( function($) {
            $('.buy-slider.owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                nav: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    768: {
                        items:3
                    },
                    1000:{
                        items:3
                    },
                    1365:{
                        items:4
                    }
                }
            });

            $('.producto-slider .owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });


            $("#buy .next").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('next.owl.carousel');
            });
            $("#buy .prev").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('prev.owl.carousel');
            });

            var owl = $('.producto-slider .owl-carousel');
            $(".producto-slider .next").on('click', function() {
                next();
            });
            $(".producto-slider .prev").on('click', function() {
                prev();
            });

            function next(){
                owl.trigger('next.owl.carousel')
            }
            function prev(){
                owl.trigger('next.owl.carousel')
            }
        });
    </script>

@endpush
