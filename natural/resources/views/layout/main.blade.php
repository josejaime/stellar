<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Mobile First :: Disable the zooming capabilities in mobile devices -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Stellar | @yield('title')</title>

    <meta name="description" content="">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/owl/dist/assets/owl.carousel.min.css?v=1.0.0') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/owl/dist/assets/owl.theme.default.min.css?v=1.0.0') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/semantic/semantic.min.css') }}">
    <link href="{{ asset('css/main.css?v=2.6') }})" rel="stylesheet">
    {{-- <link href="{{ asset('css/home.css') }}" rel="stylesheet"> --}}

    <!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/png">
	<link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png">

	<!-- Browser Toolbar Color -->
    <meta name="theme-color" content="#428599">

	@section('head_styles')

</head>
<body>
    <div class="ui sidebar inverted vertical menu">
        <a class="item" href="{{ route('que_es') }}">¿Qué es?</a>
        <a class="item" href="{{ route('stevia') }}">Stevia</a>
        <a class="item" href="{{ route('nutricion') }}">Nutrición y salud</a>
        <a class="item" href="{{ route('donde') }}">¿Dónde comprar?</a>
        <a class="item" href="{{ route('contacto') }}">Contacto</a>
        <a class="item" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar" target="_blank" class="amazon">
            <img src="{{ asset('img/boton_amazon_2.png') }}" alt="">
        </a>
    </div>
    <div id="app" class="pusher">

        <header>
            <div class="nextevia">
                <a href="#!">
                    <img src="{{ asset('img/nextevia_logo.png') }}" alt="Nextevia Logo" class="logo">
                </a>
            </div>
            <div class="natural">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('img/logo-stellar.png') }}" alt="Natural Fit Logo" class="logo">
                </a>
                <div>
                    <a href="https://www.facebook.com/Natural-Fit-1653029765027422/" target="_blank"><i class="fab fa-2x fa-facebook"></i></a>
                    <a href="https://www.instagram.com/stevianaturalfit/" target="_blank"><i class="fab fa-2x fa-instagram"></i></a>
                    <a href="https://twitter.com/naturalfitmx" target="_blank"><i class="fab fa-2x fa-twitter"></i></a>
                </div>
            </div>
        </header>

        <main>
            <nav class="menu">
                <ul class="desktop">
                    <li><a href="{{ route('que_es') }}">¿Qué es?</a></li>
                    <li><a href="{{ route('stevia') }}">Stevia</a></li>
                    <li><a href="{{ route('nutricion') }}">Nutrición y salud</a></li>
                    <li><a href="{{ route('donde') }}">¿Dónde comprar?</a></li>
                    <li><a href="{{ route('contacto') }}">Contacto</a></li>
                    <li class="trigger">
                        <a href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar" target="_blank" class="amazon">
                            <img src="{{ asset('img/boton_amazon_2.png') }}" alt="">
                        </a>
                        {{-- <ul class="second">
                            <li><a target="_blank" href="https://www.amazon.com.mx/s?k=Natural+Fit%2C+Aut%C3%A9ntico+Endulzante+de+Stevia&__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss">Amazon</a></li>
                            <li><a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar">Walmart</a></li>
                            <li><a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html">GNC</a></li>
                            <li><a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html">HEB</a></li>
                            <li><a target="_blank" href=" https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit">La Comer</a></li>
                            <li><a target="_blank" href="https://www.merco.mx/">Merco</a></li>
                            <li><a target="_blank" href="http://www.superdelnorte.com.mx/">Santovalle</a></li>
                            <li><a target="_blank" href="https://www.supermercadossmart.com/">S-Mart</a></li>
                            <li><a target="_blank" href="https://www.soriana.com/soriana/es/">Soriana</a></li>
                            <li><a target="_blank" href="http://sumesa.com.mx/sucursales.php">SUMESA</a></li>
                            <li><a target="_blank" href="https://www.casaley.com.mx/">LEY</a></li>
                            <li><a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+">Al Super</a></li>
                            <li><a target="_blank" href="http://www.casaayala.mx/">Casa Ayala</a></li>
                            <li><a target="_blank" href="https://abarrotescasavargas.mx/">Casa Vargas</a></li>
                        </ul> --}}
                    </li>
                </ul>

                <ul class="mobile">
                    <li>
                        <a href="#!" class="mobile-trigger">
                            <i class="fa fa-bars fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </nav>


            @yield('content')

        </main>
        <footer>
            <div class="natural">
                <a href="#!">
                    <img src="{{ asset('img/logo-stellar-blanco.png') }}" alt="Natural Fit Logo" class="logo">
                </a>
                <div>
                    <a target="_blank" href="https://www.facebook.com/Natural-Fit-1653029765027422/"><i class="fab fa-2x fa-facebook"></i></a>
                    <a target="_blank" href="https://www.instagram.com/stevianaturalfit/"><i class="fab fa-2x fa-instagram"></i></a>
                    <a target="_blank" href="https://twitter.com/naturalfitmx"><i class="fab fa-2x fa-twitter"></i></a>
                </div>
            </div>
        </footer>
        <div class="preloader">
            <img src="{{ asset('img/logo-stellar.png') }}" alt="Natural Fit Logo" class="logo">
        </div>
    </div>

    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/brands.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/fontawesome/css/solid.css') }}">
    <link rel="stylesheet" href="{{ asset('lib/aos/dist/aos.css?v=1.0.0') }}">


    <script src="{{ asset('lib/jquery/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('lib/owl/dist/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('lib/aos/dist/aos.js?v=1.0.0') }}"></script>
    <script src="{{ asset('lib/semantic/semantic.min.js') }}"></script>
    <script type="text/javascript">
        jQuery( function($) {

            $(window).on('load', function() {
                setTimeout(function() {
                    $('.preloader').fadeOut();
                }, 3000);
            });

            /// If it resists to disapear
            setTimeout(function() {
                /// Force it
                $('.preloader').fadeOut();
            }, 4500);
        } )
    </script>
    <script defer>

        $( document ).ready( function() {

            $(".mobile-trigger").on('click', function(){
                $('.ui.sidebar')
                .sidebar('toggle')
                ;
            });

        });
    </script>

    @stack('scripts')
    @stack('styles')
</body>
</html>
