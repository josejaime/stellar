@extends('layout.main')

@section('title', 'Contacto')

@section('content')

<!-- ======================================================= -->
<section id="contact" class="main-section">
    <div class="title">
        <h1 class="front">Contacto</h1>
    </div>
</section>
<section id="form">

    <form action="{{ route('contacto') }}" class="ui form" method="post">

        @csrf

        <div class="field">
            <label>Nombre</label>
            <input type="text" name="name" placeholder="Nombre...">
        </div>

        <div class="field">
            {{-- <label>Upper Fields</label> --}}
            <div class="two fields">
                <div class="field">
                    <label>Télefono</label>
                    <input type="text" name="phone" placeholder="Teléfono...">
                </div>
                <div class="field">
                    <label>Ciudad</label>
                    <input type="text" name="city" placeholder="Ciudad...">
                </div>
            </div>
        </div>
        <div class="ui form">
            <div class="field">
                <label>Mensaje</label>
                <textarea name="message"></textarea>
            </div>
        </div>
        <div class="field">

            <button class="ui button" type="submit">Enviar</button>
        </div>
    </form>


</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        jQuery( document ).ready( function($) {
            $('.buy-slider.owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                nav: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    768: {
                        items:3
                    },
                    1000:{
                        items:3
                    },
                    1365:{
                        items:4
                    }
                }
            });

            $('.producto-slider .owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });


            $("#buy .next").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('next.owl.carousel');
            });
            $("#buy .prev").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('prev.owl.carousel');
            });

            var owl = $('.producto-slider .owl-carousel');
            $(".producto-slider .next").on('click', function() {
                next();
            });
            $(".producto-slider .prev").on('click', function() {
                prev();
            });

            function next(){
                owl.trigger('next.owl.carousel')
            }
            function prev(){
                owl.trigger('next.owl.carousel')
            }
        });
    </script>

@endpush
