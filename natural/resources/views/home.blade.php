@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
{{-- <section class="banner">
    <h1>
        <i class="fas fa-cart-arrow-down"></i>
        Compra Ahora</h1>
    <ul class="drop-down" style="display: none;">
        <li><a target="_blank" href="https://www.amazon.com.mx/s?k=Natural+Fit%2C+Aut%C3%A9ntico+Endulzante+de+Stevia&__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss">Amazon</a></li>
        <li><a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar">Walmart</a></li>
        <li><a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html">GNC</a></li>
        <li><a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html">HEB</a></li>
        <li><a target="_blank" href=" https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit">La Comer</a></li>
        <li><a target="_blank" href="https://www.merco.mx/">Merco</a></li>
        <li><a target="_blank" href="http://www.superdelnorte.com.mx/">Santovalle</a></li>
        <li><a target="_blank" href="https://www.supermercadossmart.com/">S-Mart</a></li>
        <li><a target="_blank" href="https://www.soriana.com/soriana/es/">Soriana</a></li>
        <li><a target="_blank" href="http://sumesa.com.mx/sucursales.php">SUMESA</a></li>
        <li><a target="_blank" href="https://www.casaley.com.mx/">LEY</a></li>
        <li><a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+">Al Super</a></li>
        <li><a target="_blank" href="http://www.casaayala.mx/">Casa Ayala</a></li>
        <li><a target="_blank" href="https://abarrotescasavargas.mx/">Casa Vargas</a></li>
    </ul>
</section> --}}
<section id="home-slider" class="main-section">
    <div class="owl-carousel">
        <div class="slide" style="background-image: url('{{ asset('img/slider1.png') }}');">
            <a href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar" target="_blank"><img src="{{ asset('img/slider1.png') }}" alt=""></a>
        </div>
        <div class="slide" style="background-image: url('{{ asset('img/slider2.png') }}');">
            <a href="https://www.amazon.com.mx/s?k=Natural+Fit%2C+Aut%C3%A9ntico+Endulzante+de+Stevia&__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss" target="_blank"><img src="{{ asset('img/slider2.png') }}" alt=""></a>
        </div>
        <div class="slide" style="background-image: url('{{ asset('img/slider3.png') }}');">
            <img src="{{ asset('img/slider3.png') }}" alt="">
        </div>
    </div>
</section>
<section id="buy" data-aos="fade-up">
    <div class="title">
        <h1>Compra en</h1>
    </div>
    <div class="buy">
        <div class="prev-nav">
            <a href="#!" class="prev"><i class="fas fa-3x fa-chevron-left"></i></a>
        </div>
        <div class="buy-slider owl-carousel">
            <div class="slide">
                <a target="_blank" href="https://www.amazon.com.mx/s?k=Natural+Fit%2C+Aut%C3%A9ntico+Endulzante+de+Stevia&__mk_es_MX=%C3%85M%C3%85%C5%BD%C3%95%C3%91&ref=nb_sb_noss"><img src="{{ asset('img/amazon.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://super.walmart.com.mx/productos?Ntt=Sustituto%20natural%20Natural%20Fit%20del%20az%C3%BAcar"><img src="{{ asset('img/walmart.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://gnc.com.mx/real-natural-organics-stevia-60paquetes.html"><img src="{{ asset('img/gnc.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/grand.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.heb.com.mx/natural-fit-endulzante-stevia-natural-96-gr-662722.html"><img src="{{ asset('img/heb.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href=" https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=137&ver=mislistas&succFmt=100&criterio=Endulzante+de+stevia+natural+fit#/Endulzante%20de%20stevia%20natural%20fit"><img src="{{ asset('img/logo-la-comer.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.merco.mx/"><img src="{{ asset('img/merco.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.superdelnorte.com.mx/"><img src="{{ asset('img/santovalle.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.supermercadossmart.com/"><img src="{{ asset('img/smart-logo.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.soriana.com/soriana/es/"><img src="{{ asset('img/soriana.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://sumesa.com.mx/sucursales.php"><img src="{{ asset('img/sumesa.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/vimark.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.casaley.com.mx/"><img src="{{ asset('img/ley.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://alsuper.com/buscar?q=natural+fit+"><img src="{{ asset('img/al-super.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/avila.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="http://www.casaayala.mx/#"><img src="{{ asset('img/casa-ayala.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://abarrotescasavargas.mx/"><img src="{{ asset('img/casa-vargas.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="https://www.lacomer.com.mx/lacomer/goBusqueda.action?succId=406&ver=mislistas&succFmt=100&criterio=natural+fit#/natural%20fit"><img src="{{ asset('img/logo-fresko.png') }}" alt=""></a>
            </div>
            <div class="slide">
                <a target="_blank" href="#!"><img src="{{ asset('img/san-borja.png') }}" alt=""></a>
            </div>
        </div>
        <div class="next-nav">

            <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>
        </div>
    </div>
</section>
<section id="endulza">
    <div class="text" data-aos="flip-left">
        <img src="{{ asset('img/leaves.png') }}" alt="">
        <h1 class="first">Endulza</h1>
        <h1 class="second">Tu vida</h1>
        <h1 class="third">Con Stellar</h1>
    </div>
    <div class="image" data-aos="flip-right">
        <img src="{{ asset('img/chica-1-home.png') }}" alt="">
    </div>
</section>
<section id="productos">
    <div class="title" data-aos="flip-left">
        {{-- <div class="behind">
            <h1>Productos</h1>
        </div> --}}
        <h1 class="front">
            <img src="{{ asset('img/circle_side.png') }}" alt="">
            Productos
            <img src="{{ asset('img/circle_side.png') }}" alt="">
        </h1>
    </div>
    <div id="productos-halfs" data-aos="fade-up">

            {{-- <div class="producto-nav">
            </div> --}}

        <a href="#!" class="prev"><i class="fas fa-3x fa-chevron-left"></i></a>
        <div class="producto-slider">
            <div class="owl-carousel">
                <div class="slide">

                    <img src="{{ asset('img/stellar40.png') }}" alt="">

                    <div class="subtitle">
                        <h3>Caja 40 Sobres</h3>
                        <a href="#!" target="_blank" class="button cancan">Comprar</a>
                    </div>
                </div>
                <div class="slide">

                    <img src="{{ asset('img/stellar100.png') }}" alt="">

                    <div class="subtitle">
                        <h3>Caja 100 Sobres</h3>
                        <a href="#!" class="button cancan">Comprar</a>
                    </div>
                </div>
                <div class="slide">

                    <img src="{{ asset('img/stellar500.png') }}" alt="">

                    <div class="subtitle">
                        <h3>Caja 500 Sobres</h3>
                        <a href="#!" class="button cancan">Comprar</a>
                    </div>
                </div>
                <div class="slide">

                    <img src="{{ asset('img/stellar2000.png') }}" alt="">

                    <div class="subtitle">
                        <h3>Caja 2000 Sobres</h3>
                        <a href="#!" class="button cancan">Comprar</a>
                    </div>
                </div>
            </div>
        </div>

        <a href="#!" class="next"><i class="fas fa-3x fa-chevron-right"></i></a>

    </div>
</section>

<section id="chica2">
    <div class="image">
        <img src="{{ asset('img/chica2-home.png') }}" alt="">
        <h1>Dulce y <span>Natural</span></h1>
    </div>
</section>
{{-- <section id="recetas">
    <div class="title">
        <h1 class="front">Recetas</h1>
    </div>
    <div id="recepies">
        <a href="#!" class="recipe" id="receta1">
            <div>
                <img src="{{ asset('img/receta_1.jpg') }}" alt="">
            </div>
            <div class="content">
                <h4>Agua de piña, apio y zanahoria</h4>

            </div>
        </a>
        <a href="#!" class="recipe" id="receta2">
            <div>
                <img src="{{ asset('img/receta_2.jpg') }}" alt="">
            </div>
            <div class="content">
                <h4>Frappé de café</h4>

            </div>
        </a>
        <a href="#!" class="recipe" id="receta3">
            <div>
                <img src="{{ asset('img/receta_3.jpg') }}" alt="">
            </div>
            <div class="content">
                <h4>Agua de fresa con hierbabuena y agua de sandía con chia</h4>

            </div>
        </a>
    </div>
</section> --}}

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/home.css?v=2.2') }}">
@endpush

@push('scripts')
    <script>
        // AOS.init();
        $(function() {
          AOS.init();
          window.addEventListener('load', AOS.refresh)
        });
    </script>
    <script>
        jQuery(function($) {

            $(".banner h1").on('click', function(){
                $(".banner ul").toggle("slow");
            });

            $('.ui.modal')
                .modal()
            ;

            $('#home-slider .owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                autoplay: true,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });
            $('.producto-slider .owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });

            $('.buy-slider.owl-carousel').owlCarousel({
                loop: true,
                margin: 20,
                nav: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    768: {
                        items:3
                    },
                    1000:{
                        items:3
                    },
                    1365:{
                        items:4
                    }
                }
            });


            var owl = $('.producto-slider .owl-carousel');
            $("#productos-halfs .next").on('click', function() {
                next();
            });
            $("#productos-halfs .prev").on('click', function() {
                prev();
            });

            $("#buy .next").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('prev.owl.carousel');
            });
            $("#buy .prev").on('click', function() {
                $('.buy-slider.owl-carousel').trigger('next.owl.carousel');
            });

            function next(){
                owl.trigger('next.owl.carousel')
            }
            function prev(){
                owl.trigger('prev.owl.carousel')
            }

        });
    </script>
@endpush
                                                                                                                                                                                                                                                                                        
