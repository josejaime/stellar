@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
<section id="stevia" class="main-section">
    <div class="title">
        <h1 class="front">Stevia</h1>
    </div>
</section>
<section data-aos="fade-up" id="subtitle">
    <p>
        Ningún endulzante supera el <span class="green">sabor rico</span> y
        <span class="green">dulce</span> de la <span class="green">Stevia</span>
    </p>
</section>
<section id="plant">
    {{-- <div class="">
        <h1>¿Qué es?</h1>
        <p>
            La stevia (Stevia rebaudiana Bertoni) es una planta originaria de
            Paraguay. Contiene cero calorías.
        </p>
    </div> --}}
    <div class="plant">
        <img src="{{ asset('img/stevia_planta.png') }}" alt="" data-aos="zoom-in-down">

        <div class="">
            <h2>¿Qué es?</h2>
            <p>
                La stevia (Stevia rebaudiana Bertoni) es una planta originaria de
                Paraguay. Contiene cero calorías.
            </p>
        </div>

        <div class="">
            <h2>Historia</h2>
            <p>
                Desde hace cientos de años se ha utilizado como endulzante en las
                comunidades Guaraníes de Sudamerica. En Japón se utiliza desde los
                años 70´s. En 2011 llega a los supermercados de México.
            </p>
        </div>

        <div class="">
            <h2>¿Cómo saber cual es auténtica?</h2>
            <p>
                Las hojas de Stevia están compuestas por componentes dulces y
                componentes amargos, el secreto está en extraer solo sus componentes
                dulces. Muchas marcas de Stevia no logran deshacerse de sus
                componentes amargos en su proceso de extracción lo cual
                resulta en un sabor amargo, por eso recurren a enmascarar
                ese mal sabor con otros endulzantes sintéticos y
                al final resulta una revoltura que es todo menos natural.
            </p>
        </div>

        <div class="">
            <h2>Nuestro Método</h2>
            <p>
                Para lograr extraer el sabor dulce de nuestras hojas de stevia
                utilizamos un proceso con únicamente agua natural, el resultado es
                un sabor rico y 100% natural elaborado desde la siembra hasta
                la extracción en suelo mexicano.
            </p>
        </div>
        <div class="spacer" style="clear:both;"></div>
    </div>
    {{-- <div class="">
        <h1>Nuestro Método</h1>
        <p>
            Para lograr extraer el sabor dulce de nuestras hojas de stevia
            utilizamos un proceso con únicamente agua natural, el resultado es
            un sabor rico y 100% natural elaborado desde la siembra hasta
            la extracción en suelo mexicano.
        </p>
    </div> --}}
</section>

<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/stevia.css?v=2.1') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">
        $(".accordion").on('click', function() {
            console.log("clicked");
            $(this).children('span').toggle('slow');
            $(this).children('p').children('i').toggleClass('active');
        });
    </script>

@endpush
