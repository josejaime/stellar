@extends('layout.main')

@section('title', 'HOME')

@section('content')

<!-- ======================================================= -->
<section id="whatis" class="main-section">
    <div class="title">
        <h1 class="front">¿Qué es?</h1>
    </div>
</section>
<section id="texts">
    <img src="{{ asset('img/leaves.png') }}" alt="" class="leaves">
    <div class="content">
        <div class="single">
            <h2>100% Natural</h2>
            <p>
                Nuestro sistema patentado de extracción a base de agua hace que
                Stellar Stevia sea 100% natural, ya que no contiene aditivos
                artificiales ni componentes químicos.
            </p>
        </div>
    </div>
    <div class="content">
        <div class="double">
            <h2>Cero Calorías</h2>
            <p>
                Debido a que no contiene azúcar es Cero Calorías.
            </p>
        </div>
        <div class="double">
            <h2>5% de Stevia</h2>
            <p>
                Stellar Stevia contiene un 20% más de Stevia que cualquier
                otra marca.
            </p>
        </div>
    </div>
    <img src="{{ asset('img/sobre-stellar.png') }}" alt="" class="sobre">
    <div class="content">
        <div class="single">
            <h2>Para toda la familia</h2>
            <p>
                Debido a su bajo contenido glicémico Stellar Stevia es
                recomendable para toda la familia incluyendo niños,
                mujeres embarazadas y diabéticos.
            </p>
        </div>
    </div>
    <div class="content">
        <div class="double">
            <h2>Apto para diabéticos</h2>
            <p>
                Recomendado por la Asociación Mexicana de Diabetes.
            </p>
        </div>
        <div class="double">
            <h2>Cultivada en México</h2>
            <p>
                Nuestra Stevia es orgullosamente cultivada y procesada en
                Sonora, México.
            </p>
        </div>
    </div>
</section>
<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/whatis.css') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>

@endpush
