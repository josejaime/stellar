@extends('layout.main')

@section('title', 'Nutrición')

@section('content')

<!-- ======================================================= -->
{{-- <section id="nutrition" class="main-section">
    <div class="title">
        <h1 class="front">Stevia</h1>
    </div>
    <div class="image">
        <img src="{{ asset('img/sobre-vertical.png') }}" alt="">
    </div>

</section> --}}
<section id="nutrition">
    <img src="{{ asset('img/sobre-vertical.png') }}" alt="" class="image">

    <div class="questions">
        <div class="">
            <h2>¿Qué edulcorantes existen?</h2>
            <p>
                En México, el 97% de los endulzantes que consumimos es azúcar de caña
                y el resto está dividido entre muchos edulcorantes de alta intensidad
                (EAI) bajos en calorías.
            </p>
        </div>
        <div class="">
            <h2>¿Cuáles son los más comunes?</h2>
            <p>
                Los EAI que más se conocen en nuestro país son, Sacarina, Aspartame,
                Sucralosa, y Stevia (Stellar).
            </p>
        </div>
        <div class="">
            <h2>¿Qué es Stevia?</h2>
            <p>
                De todos los EAI que existen la Stevia es Un edulcorante natural
                proveniente de la hoja de una planta del mismo nombre y que se
                cultiva en México desde hace muchos años.
            </p>
        </div>
        <div class="">
            <h2>¿Por qué Stellar?</h2>
            <p>
                Al día de hoy, Stellar® es de los únicos productos en el mercado con Cero
                Azúcar, pues el resto contiene algún tipo de azúcar y/o edulcorante
                artificial lo que los hace NO apto para diabéticos.
            </p>
        </div>
        <div class="">
            <h2>¿Qué significa 5% de Stevia?</h2>
            <p>
                Significa que de todo el peso del producto un 5% es Stevia,
                extracto puro de la planta. El resto de las marcas tiene
                menos de 3.5%.
            </p>
        </div>
        <div class="">
            <h2>¿Y el 95% restante qué es?</h2>
            <p>
                Se llama excipiente y en nuestro caso es Maltodextrina, es un
                polvo blanco que funciona como agente de volumen para darle mayor
                practicidad en su aplicación.
            </p>
        </div>
        <div class="">
            <h2>¿Por qué ese precio?</h2>
            <p>
                Stellar® tiene un poder edulcorante de 14x es decir que 5 gramos de
                producto endulza el equivalente a 70 gramos de azúcar de caña
                pero sin las calorías. 1 sobre igual a dos cucharaditas de azúcar.
            </p>
        </div>
        <div class="">
            <h2>¿Qué efecto tiene sobre mí?</h2>
            <p>
                Stellar® no se digiere en nuestro sistema gastro intestinal y se
                excreta por la orina no afectando nuestra micro biota (bacterias buenas).
                Y como no se absorbe no eleva los niveles de azúcar en sangre.
            </p>
        </div>
        <div class="">
            <h2>¿Me ayuda a mejorar mi rendimiento?</h2>
            <p>
                Stellar® no se digiere en nuestro sistema gastro intestinal y se
                excreta por la orina no afectando nuestra micro biota
                (bacterias buenas). Y como no se absorbe no eleva los niveles
                de azúcar en sangre.
            </p>
        </div>
    </div>
</section>
<section id="iconos">
    <div class="">
        <img src="{{ asset('img/icono-planta.png') }}" alt="">
    </div>
    <div class="">
        <img src="{{ asset('img/icono-corazon.png') }}" alt="">
    </div>
    <div class="">
        <img src="{{ asset('img/sobres.png') }}" alt="">
    </div>
    <div class="">
        <img src="{{ asset('img/icono-familia.png') }}" alt="">
    </div>
    <div class="">
        <img src="{{ asset('img/icono-monedas.png') }}" alt="">
    </div>
</section>
<!-- ======================================================* -->

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/nutrition.css?v=3') }}">
@endpush

@push('scripts')
    <script>
        AOS.init();
    </script>
    <script type="text/javascript">

        $(document).ready( function(){

            $('.slider.owl-carousel').owlCarousel({
                loop:true,
                margin:0,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            });
        });
    </script>

@endpush
