<?php

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('layout.main');
    return view('home');
})->name('home');
Route::get('/nutricion', function () {
    return view('nutrition');
})->name('nutricion');
Route::get('/stevia', function () {
    return view('stevia');
})->name('stevia');
Route::get('/que-es', function () {
    return view('que_es');
})->name('que_es');
Route::get('/donde-comprar', function () {
    return view('donde');
})->name('donde');
Route::get('/contacto', function () {
    return view('contacto');
})->name('contacto');
Route::post('/contacto', function (Request $request) {
    //return request('name');
    Mail::send(new ContactMail($request));
    return redirect('/');
})->name('contacto');
